#include "OTA_Task.h"
extern uint8_t AT_rev[1200]; //全局缓存，接收消息队列前清除，如需处理，在接收后处理即可  Queue_Size必须 >= 消息队列大小
extern osThreadId OTA_TaskHandle;
extern osSemaphoreId OTA_StartHandle;
extern osThreadId defaultTaskHandle;
extern osThreadId EC600S_TaskHandle;
extern osThreadId Sensor_TaskHandle;
void OTA_Task(void)
{
    uint8_t ucExecRes = 0;
    uint16_t bin_addr = 0;     //文件地址指针偏移量
    uint16_t read_size = 1024; //每次读1K 最后一页需要计算
    uint16_t end_size = 0;     //最后一包的大小
    uint8_t  page_number = 0;  //总包数
    uint8_t  now_number = 0;   //当前包序号
    printf("已经切换到OTA任务..挂起其他无关任务.....\r\n");
    vTaskSuspend(EC600S_TaskHandle);     //
    vTaskSuspend(Sensor_TaskHandle);     //
    //清空100K 数据
    printf("开始擦除APP区域.90K..\r\n");
    /* 擦除App2     2K 一页*/
    Erase_page(Application_2_Addr, 45); //擦除45页  90K
    //打开UFS 文件系统
    end_size = EC600S_type.BIN_len%1024; //最后一包大小
    if(end_size == 0) //恰巧是完整包
    {
        page_number = EC600S_type.BIN_len/1024;
    } else page_number = EC600S_type.BIN_len/1024+1;
    printf("总长：%d 总包：%d 末包：%d\r\n",EC600S_type.BIN_len,page_number,end_size);
    while(1)
    {
        if(bin_addr == 0) //首次读取
        {
            //打开文件
            ucExecRes= EC600S_AT_SR("AT+QFOPEN=\"ota.bin\",0\r\n","+QFOPEN: 1","ERROR",2000/portTICK_RATE_MS); //返回值为文件句柄 当操作文件比较多时有可能是变量
            if(ucExecRes ==ExecFail)
            {
                printf("打开文件系统异常 ... \r\n");
                break;
            }
        }
        //设置文件指针位置
        memset(AT_send,0x00,sizeof((char *)AT_send));
        sprintf((char *)AT_send,"AT+QFSEEK=1,%d,0\r\n",bin_addr);
        ucExecRes= EC600S_AT_SR((char *)AT_send,"OK","ERROR",1000/portTICK_RATE_MS); //
        if(ucExecRes ==Normal)
        {
            //读文件
            memset(AT_send,0x00,sizeof((char *)AT_send));
            sprintf((char *)AT_send,"AT+QFREAD=1,%d\r\n",read_size);
            ucExecRes= EC600S_AT_SR((char *)AT_send,"OK\r\n","ERROR",1000/portTICK_RATE_MS); //
            if(ucExecRes ==Normal)
            {
               // memset(bin_file,0x00,sizeof((char *)bin_file));
               // Find_string((char *)AT_rev+4,"\r\n","\r\nOK",(char *)bin_file); //取数据这里可以再优化下
							printf("*******************\r\n");
                for(int i =0; i<1024; i++)
                {
                    printf("%x ", AT_rev[i]);
                }
							printf("*******************\r\n");
                //开始烧写到内存
								printf("当前包：%d,总长：%d 总包：%d 末包：%d\r\n",now_number,EC600S_type.BIN_len,page_number,end_size);
                WriteFlash(Application_2_Addr+bin_addr, AT_rev,read_size);
                now_number+= 1;
                if ((now_number == page_number-1)&&(end_size != 0) ) //存在最后不整包，并且到达了最后一包
                {
                    bin_addr+= 1024;//把读过的地址加上
                    read_size = end_size;
                } else if(now_number == page_number)
                {
                    printf("读包结束....\r\n");
                    printf("固件接收完成....j即将重启..\r\n");
                    osDelay(2000);
                    Set_Update_Down(); //记录更新标志位
                    HAL_NVIC_SystemReset(); //重启系统
                    while(1) {}
                } else
                {
                    bin_addr  +=1024;
                    read_size = 1024;
                }
            } else
            {
                printf("未获取到文件....\r\n");
            }
        }
    }
}

