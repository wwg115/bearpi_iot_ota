#include "EC600S.h"

uint8_t AT_rev[1200]; //全局缓存，接收消息队列前清除，如需处理，在接收后处理即可  Queue_Size必须 >= 消息队列大小
uint8_t AT_send[200];
extern osThreadId EC600S_TaskHandle;
extern osMessageQId EC600S_Rev_QueueHandle;
extern osThreadId OTA_TaskHandle;
extern osSemaphoreId OTA_StartHandle;
cat_1_type EC600S_type;

//等待开机
uint8_t EC600S_Wait_Wakeup(void)
{
    uint8_t ucExecRes = 0;
	 //首先复位模组，由于Cat.1模组的复位引脚与主板复位引脚共用，所以会导致主板复位。
	  ucExecRes= EC600S_AT_SR("AT+CFUN=1,1\r\n","RDY","ERROR",1000/portTICK_RATE_MS);
    ucExecRes= EC600S_AT_SR("AT\r\n","RDY","ERROR",10000/portTICK_RATE_MS);
    return ucExecRes;
}
//关闭回显
uint8_t EC600S_ATE0_EN(void)
{
    uint8_t ucExecRes = 0;
    ucExecRes= EC600S_AT_SR("ATE0\r\n","OK","ERROR",1000/portTICK_RATE_MS);
    return ucExecRes;
}
//等待设备驻网 4G模组都是自动驻网的
uint8_t EC600S_WAIT_CREG(void)
{
    uint8_t ucExecRes = 0;
    ucExecRes= EC600S_AT_SR("AT+CREG?\r\n","+CREG: 0,1","+CREG: 0,2",1000/portTICK_RATE_MS);
    return ucExecRes;
}
//获取设备 CSQ信号值
uint8_t EC600S_GET_CSQ(void)
{
    uint8_t ucExecRes = 0;
    uint8_t CSQ_buf[2] = {0};
    ucExecRes= EC600S_AT_SR("AT+CSQ\r\n","+CSQ","ERROR",1000/portTICK_RATE_MS);
    if(ucExecRes ==Normal)
    {
        memset(CSQ_buf,0x00,sizeof(CSQ_buf));
        if(Find_string((char *)AT_rev,"+CSQ: ",",",(char *)CSQ_buf))
            EC600S_type.CSQ = atoi((char *)(CSQ_buf));
        else EC600S_type.CSQ =0;
        printf("CSQ:%d\r\n", EC600S_type.CSQ);
        if(EC600S_type.CSQ == 99)
        {
            return 99;
        }
    }
    return ucExecRes;
}
//获取设备IMEI-唯一身份标识
uint8_t EC600S_GET_IMEI(void)
{
    uint8_t ucExecRes = 0;
    ucExecRes= EC600S_AT_SR("AT+GSN\r\n","OK","ERROR",1000/portTICK_RATE_MS);
    if(ucExecRes ==Normal)
    {
        memset(EC600S_type.IMEI,0x00,sizeof(EC600S_type.IMEI));
        snprintf((char *)EC600S_type.IMEI,16,"%s",AT_rev+2);
        printf("IMEI:%s\r\n", EC600S_type.IMEI);
    }
    return ucExecRes;
}
//获取平台端的设备版本
uint8_t EC600S_GET_Version(uint8_t *Server_address)
{
    uint8_t ucExecRes = 0;
    uint8_t http_rev[500]; //HTTP接收文件暂存区
    char server_version_str[4];
    cJSON *json,*jsonArray,*jsonTemp;
    ucExecRes= EC600S_AT_SR("AT+QHTTPSTOP\r\n","OK","ERROR",1000/portTICK_RATE_MS); //先断开连接
    ucExecRes= EC600S_AT_SR("AT+QHTTPCFG=\"contextid\",1\r\n","OK","ERROR",1000/portTICK_RATE_MS); //配置上下文ID
    ucExecRes= EC600S_AT_SR("AT+QHTTPCFG=\"responseheader\",0\r\n","OK","ERROR",1000/portTICK_RATE_MS); //关闭HTTP响应头
    ucExecRes= EC600S_AT_SR("AT+QICSGP=1,1,\"CMNET\",\"\",\"\" ,1\r\n","OK","ERROR",1000/portTICK_RATE_MS); //设置APN  根据自己卡类型修改
    ucExecRes= EC600S_AT_SR("AT+QIACT?\r\n","+QIACT:","OK",1000/portTICK_RATE_MS); //查询连接状态  如果已经连接不再连接
    if(ucExecRes ==ExecFail) //
    {
        ucExecRes= EC600S_AT_SR("AT+QIACT=1\r\n","OK","ERROR",1000/portTICK_RATE_MS); //
        if(ucExecRes ==Normal)
        {
            ucExecRes= EC600S_AT_SR("AT+QIACT?\r\n","+QIACT:","OK",1000/portTICK_RATE_MS); // 再次查询连接状态
            if(ucExecRes ==ExecFail)
            {
                return ExecFail;
            }
        }
    }
    memset(AT_send,0x00,sizeof((char *)AT_send));
    sprintf((char *)AT_send,"AT+QHTTPURL=%d,120\r\n",strlen((char *)Server_address)); //发送连接长度
    ucExecRes= EC600S_AT_SR((char *)AT_send,"CONNECT","ERROR",1000/portTICK_RATE_MS);
    if(ucExecRes ==Normal)
    {
        //发送请求的连接
        ucExecRes= EC600S_AT_SR((char *)Server_address,"OK","ERROR",1000/portTICK_RATE_MS);
        if(ucExecRes ==Normal)
        {
            //发起请求
            ucExecRes= EC600S_AT_SR("AT+QHTTPGET=80\r\n","+QHTTPGET: 0,200","ERROR",1000/portTICK_RATE_MS);
            if(ucExecRes ==Normal)
            {
                //直接把Json的数据包 读出来
                ucExecRes= EC600S_AT_SR("AT+QHTTPREAD=200\r\n","CONNECT","ERROR",1000/portTICK_RATE_MS);

                memset(http_rev,0x00,sizeof(http_rev));
                Find_string((char *)AT_rev,"CONNECT\r\n","\r\nOK",(char *)http_rev);
                printf("%s\r\n",http_rev);
                ucExecRes= EC600S_AT_SR("AT+QHTTPSTOP\r\n","OK","ERROR",1000/portTICK_RATE_MS); //断开连接
                json=cJSON_Parse((char *)http_rev);
                if (!json) {
                    printf("Error before: [%s]\n",cJSON_GetErrorPtr());
                    cJSON_Delete(json);
                    free(json);
                }
                else {
                    jsonArray=cJSON_GetArrayItem(json,0); //取数组
                    jsonTemp =cJSON_GetObjectItem(jsonArray,"bin_version");
                    memset(server_version_str,0x00,sizeof(server_version_str));
                    sprintf(server_version_str,"%s",jsonTemp->valuestring);
                    cJSON_Delete(json);
                    free(json);
                    EC600S_type.Server_version = atoi(server_version_str);
                    if(EC600S_type.Server_version>EC600S_type.Board_version)
                    {
                        printf("有新版本.....\r\n");
                        return 66;
                    }else
										{
										    printf("未发现新版本....\r\n");
										}
                }
            }
        }
    }
    return ucExecRes;
}

//获取平台端对应IMEI的BIN文件
uint8_t EC600S_Get_Bin(uint8_t *Server_address)
{
    uint8_t ucExecRes = 0;
    uint8_t http_rev[100]; //HTTP接收文件暂存区
    uint8_t UFS_bin_len[10];
    memset(AT_send,0x00,sizeof((char *)AT_send));
    sprintf((char *)AT_send,"AT+QHTTPURL=%d,120\r\n",strlen((char *)Server_address)); //发送连接长度
    ucExecRes= EC600S_AT_SR((char *)AT_send,"CONNECT","ERROR",1000/portTICK_RATE_MS);
    if(ucExecRes ==Normal)
    {
        //发送请求的连接
        ucExecRes= EC600S_AT_SR((char *)Server_address,"OK","ERROR",1000/portTICK_RATE_MS);
        if(ucExecRes ==Normal)
        {
            //发起请求
            ucExecRes= EC600S_AT_SR("AT+QHTTPGET=80\r\n","+QHTTPGET: 0,200","ERROR",1000/portTICK_RATE_MS);
            if(ucExecRes ==Normal)
            {
                //取出 bin文件长度 备用
                memset(http_rev,0x00,sizeof(http_rev));
                Find_string((char *)AT_rev,"+QHTTPGET: 0,200,","\r\n",(char *)http_rev);
                EC600S_type.BIN_len = atoi((char *)(http_rev));
                printf("文件长度为%d\r\n",EC600S_type.BIN_len);
                if(EC600S_type.BIN_len >1024)  //简单判断下文件大小  太小不可能是固件
                {
                    //将文件写入 UFS文件系统
                    ucExecRes= EC600S_AT_SR("AT+QHTTPREADFILE=\"UFS:ota.bin\",80\r\n","+QHTTPREADFILE: 0","ERROR",2000/portTICK_RATE_MS);
                    if(ucExecRes ==Normal)
                    {
                        //检查一下 是否写入成功  // +QFLST: "UFS:ota.bin",39744
                        ucExecRes= EC600S_AT_SR("AT+QFLST=\"*\"\r\n","+QFLST: \"UFS:ota.bin\"","ERROR",1000/portTICK_RATE_MS);
                        //再检查一下文件长度是否对应
                        if(ucExecRes ==Normal)
                        {
                            memset(UFS_bin_len,0x00,sizeof(UFS_bin_len));
                            Find_string((char *)AT_rev,"\"UFS:ota.bin\",","\r\n",(char *)UFS_bin_len);
                            uint16_t ufs_bin_len = atoi((char *)UFS_bin_len);
                            printf("UFS文件长度为:%d   %d\r\n",ufs_bin_len,EC600S_type.BIN_len);
                            if(ufs_bin_len == EC600S_type.BIN_len)
                            {
                                // 判定写入成功
                                // 挂起其他所有任务  发送OTA任务启动信号量，开始运行OTA任务
                                ucExecRes= EC600S_AT_SR("AT+QHTTPSTOP\r\n","OK","ERROR",1000/portTICK_RATE_MS); //先断开连接
                                vTaskResume(OTA_TaskHandle);  //恢复OTA任务
                                xSemaphoreGive(OTA_StartHandle); //发送信号量
                                vTaskSuspend(NULL);   //挂起自己
                                //其实返回值已经不重要了 能成功 直接就换任务了
                                ucExecRes =Normal ;
                            }
                            else {
                                printf("比对文件长度异常...\r\n");
                            }
                        } else {

                            printf("列举文件异常...\r\n");
                        }
                    }
                } else
                {
                    printf("固件包过小---请检查文件！！\r\n");
                    ucExecRes = ParaErr;
                }
            } else
            {
                printf("HTTP 请求异常 ！！！\r\n");
                ucExecRes = ParaErr;
            }
        } else
        {
            printf("发起HTTP 请求失败 ！！！\r\n");
            ucExecRes = ParaErr;
        }
    }
    return ucExecRes;
}
//连接MQTT服务器
uint8_t EC600S_Connect_MQTT(uint8_t *Server_address,uint16_t Server_port,uint8_t *clientid,uint8_t *username,uint8_t *password)
{
    uint8_t ucExecRes = 0;
    ucExecRes= EC600S_AT_SR("AT+QMTCLOSE=0\r\n","ERROR","+QMTCLOSE: 0,0",1000/portTICK_RATE_MS); //先断开MQTT连接
    memset(AT_send,0x00,sizeof((char *)AT_send));
    sprintf((char *)AT_send,"AT+QMTOPEN=0,\"%s\",%d\r\n",Server_address,Server_port); //发送连接长度
    ucExecRes= EC600S_AT_SR((char *)AT_send,"+QMTOPEN: 0,0","ERROR",1000/portTICK_RATE_MS);
    if(ucExecRes ==Normal)
    {
        memset(AT_send,0x00,sizeof((char *)AT_send));
        sprintf((char *)AT_send,"AT+QMTCONN=0,\"%s\",\"%s\",\"%s\"\r\n",clientid,username,password); //发送连接长度
        ucExecRes= EC600S_AT_SR((char *)AT_send,"+QMTCONN: 0,0,0","ERROR",1000/portTICK_RATE_MS);
    } else
    {
        printf("MQTT open ERROR !!!!\r\n");
    }
    return ucExecRes;
}
uint8_t EC600S_Subscribe_MQTT(uint8_t * Sub_Topic,uint8_t qos)
{
	 uint8_t ucExecRes = 0;
// AT+QMTSUB=0,1,"topic/pub",0
    memset(AT_send,0x00,sizeof((char *)AT_send));
    sprintf((char *)AT_send,"AT+QMTSUB=0,1,\"%s\",%d\r\n",Sub_Topic,qos); //发送连接长度
    ucExecRes= EC600S_AT_SR((char *)AT_send,"+QMTSUB: 0,1","ERROR",1000/portTICK_RATE_MS);
	  if(ucExecRes ==Normal)
		{
		 printf("订阅 %s成功....\r\n",Sub_Topic);
		}
   return ucExecRes;
}


//自检网络
uint8_t EC600S_Check_Net(void)
{
    uint8_t ucExecRes = 0;
    uint8_t CPAS_buf[2] = {0};
    //检查模组是否正常
    //0 Ready
    //2 Unknown
    //3 Ringing
    //4 Call in progress or call hold*
    ucExecRes = EC600S_GET_CSQ();  //查询信号
    if(ucExecRes ==99) return 99;
    ucExecRes= EC600S_AT_SR("AT+CPAS\r\n","+CPAS","ERROR",1000/portTICK_RATE_MS); //查询模组状态
    if(ucExecRes == Normal)
    {
        memset(CPAS_buf,0x00,sizeof(CPAS_buf));
        if(Find_string((char *)AT_rev,"+CPAS: ","\r\n",(char *)CPAS_buf)) //取出状态码
            return atoi((char *)(CPAS_buf));
        else return 100;
    } else
    {
        return 44;
    }
}
//发送对应Topic的消息
uint8_t EC600S_Publish_MQTT(uint8_t *Publish_topic,uint8_t *Message,uint8_t Csq)
{
    uint8_t ucExecRes = 0;

    memset(AT_send,0x00,sizeof((char *)AT_send));
    if(strlen((char *)Message)>2) //筛出异常数据
    {
        sprintf((char *)AT_send,"AT+QMTPUBEX=0,0,%d,0,\"%s\",%d\r\n",Csq,Publish_topic,strlen((char *)Message)); //发送连接长度
        ucExecRes= EC600S_AT_SR((char *)AT_send,">","ERROR",1000/portTICK_RATE_MS);
        if(ucExecRes ==Normal)
        {
            ucExecRes= EC600S_AT_SR((char *)Message,"+QMTPUBEX:","ERROR",2000/portTICK_RATE_MS);
        }else{
				 return ucExecRes;
				}
    } else
    {
        printf("json数据异常。。。\r\n");
    }
    printf("*******%d",ucExecRes);
    return ucExecRes;
}

//串口AT指令收发函数
uint8_t EC600S_AT_SR( char *pcCmd2Send, char *ppcSucBuf,  char *ppcErrBuf, TickType_t xTicksToWait)
{
    uint16_t res = Normal;
    memset(AT_rev,0x00,sizeof(AT_rev));
    //如果数据量大可以采用DMA发送，考虑本次数据量很小，故直接阻塞发送
    HAL_UART_Transmit(&hlpuart1,(uint8_t *)pcCmd2Send,strlen(pcCmd2Send),0xffff);
    printf("AT_SEND:%s",pcCmd2Send);
    //读3次消息队列  消费完
    for(int i =0 ; i<2; i++)  //最多把两个缓存消息队列消费完
    {
        if(xQueueReceive(EC600S_Rev_QueueHandle,AT_rev,xTicksToWait) == pdPASS)
        {
            //  uxQueueSpacesAvailable(EC600S_Rev_QueueHandle) //得到队列剩余的大小
            //  uxQueueMessagesWaiting(EC600S_Rev_QueueHandle) //得到队列使用的大小   AT+QFREAD
            //queue_remain_size = uxQueueMessagesWaiting(EC600S_Rev_QueueHandle); ///得到队列使用的大小
					  printf("AT_REV:%s\r\n",AT_rev);
            if(strstr(pcCmd2Send,"AT+QFREAD") != NULL) //如果是BIN文件请求,
            {
                if(strstr((char *)AT_rev+(AT_usartype.UsartDMARecLen-6),ppcSucBuf) != NULL) //判结尾是不是OK\r\n
                {
                    printf("BIN-ACK OK\r\n");
                    return Normal;
                }
                else if(strstr((char *)AT_rev,(char *)ppcErrBuf) != NULL)
                {
                    //包含异常接收的字符串
                    res = ExecFail;
                } else
                {
                    //包含接收其他的字符串
                    res = OtherErr;
                }

            } else
            {
                if(strstr((char *)AT_rev,ppcSucBuf) != NULL)
                {
                    //包含期待接收的字符串   直接返回
                    printf("AT-ACK OK\r\n");
                    //	free(AT_REV);
                    return Normal;
                }
                else if(strstr((char *)AT_rev,(char *)ppcErrBuf) != NULL)
                {
                    //包含异常接收的字符串
                    res = ExecFail;
                } else
                {
                    //包含异常接收其他的字符串
                    res = OtherErr;
                }
            }
        } else
        {
            printf("消费AT消息队列超时\r\n");
            res = ReceTimeOut;
        }
    }
    return res;
}

