#include "Sensor_work.h"

Sensor_type E53_Sensor_type;
uint8_t dat[2] = {0};        //dat[0]是高字节，dat[1]是低字节
uint8_t recv_dat[6] = {0};
extern osMessageQId Sensor_QueueHandle;
portBASE_TYPE err;

void Sensor_Task(void)
{
    E53_Sensor_type.Temperature = 0.0;
    E53_Sensor_type.Humidity = 0.0;
    while(1)
    {
        if(HAL_OK == BH1750_Send_Cmd(ONCE_H_MODE))
            HAL_Delay(100);
        if(HAL_OK == BH1750_Read_Dat(dat))
        {
            E53_Sensor_type.Light = BH1750_Dat_To_Lux(dat);
        }
        else
        {
            //printf("recv fail");
        }
        if(SHT30_Read_Dat(recv_dat) == HAL_OK)
        {
            if(SHT30_Dat_To_Float(recv_dat, &E53_Sensor_type.Temperature, &E53_Sensor_type.Humidity)==0)
            {
                //  printf("temperature = %f, humidity = %f\n", E53_Sensor_type.Temperature, E53_Sensor_type.Humidity);
            }
            else
            {
                printf("crc check fail.\n");
            }
        }

        //Temperature   Humidity    Light
        cJSON * pJsonmsg = NULL;
        pJsonmsg = cJSON_CreateObject();//
        cJSON_AddNumberToObject(pJsonmsg, "Temperature",E53_Sensor_type.Temperature );//
        cJSON_AddNumberToObject(pJsonmsg, "Humidity",E53_Sensor_type.Humidity );//
        cJSON_AddNumberToObject(pJsonmsg, "Light",E53_Sensor_type.Light );//
        cJSON_AddNumberToObject(pJsonmsg, "CSQ",EC600S_type.CSQ );//
        char * allJsonStr = cJSON_Print(pJsonmsg);
        cJSON_Delete(pJsonmsg);//
        free(allJsonStr);
         printf("######JSON####%s\r\n",allJsonStr);
        //发送消息入队  采集过快  消费太慢  会导致队列阻塞
            err = xQueueSendToBack(Sensor_QueueHandle,allJsonStr,1000/portTICK_RATE_MS);
            if( err== pdPASS)
            {
                // printf("发送MQTT消息队列入队成功...\r\n");
            } else if(err== pdFALSE)
            {
                // printf("MQTT消息队列已满入队失败...\r\n");
            }
        osDelay(1000); //此延时是传感器采集间隔，过短会导致消息队列拥堵，但不影响上报
    }
}

